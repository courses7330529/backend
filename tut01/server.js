const path = require('path')
const os = require('os')

const math = require('./math')
const { add } = require('./math')

console.log(math.add(2, 3));
console.log(add(2, 3));

console.log(global);

console.log(os.type());
console.log(os.version());
console.log(os.homedir());

console.log(__dirname);
console.log(__filename);

console.log(path.dirname(__filename));
console.log(path.basename(__filename));
console.log(path.extname(__filename));

console.log(path.parse(__filename));