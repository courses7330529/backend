const data = {
    employees: require('../model/employees.json'),
    setEmployees: function (data) { this.employees = data }
};


const getAllEmployees = (req, res) => {
    res.json(data.employees);
}

const createNewEmployee = (req, res) => {
    const newEmployee = {
        id: data.employees[data.employees.length - 1].id + 1 || 1,
        "firstname": req.body.firstname,
        "lastname": req.body.lastname
    }

    if (!newEmployee.firstname || !newEmployee.lastname) {
        return res.status(400).json({ 'message': 'First and last names are required.' })
    }

    data.setEmployees([...data.employees, newEmployee])
    res.json(data.employees)
}

const updateEmployee = (req, res) => {

    const employee = data.employees.find(x => x.id === parseInt(req.body.id))

    if (!employee) {
        return res.status(400).json({ 'message': `Employee ID ${req.body.id} not found` })
    }

    if (req.body.firstname) {
        employee.firstname = req.body.firstname
    }
    if (req.body.lastname) {
        employee.lastname = req.body.lastname
    }

    const filteredArr = data.employees.filter(x => x.id !== parseInt(req.bobdy.id))
    const unsortedArr = [...filteredArr, employee]
    data.setEmployees(unsortedArr((a, b) => a.id - b.id))

    res.json(data.employees)

}

const deleteEmployee = (req, res) => {
    const employee = data.employees.find(x => x.id === parseInt(req.body.id))
    if (!employee) {
        return res.status(400).json({ 'message': `Employee ID ${req.body.id} not found` })
    }

    const filteredArr = data.employees.filter(x => x.id !== parseInt(req.body.id))
    data.setEmployees([...filteredArr])
    res.json(data.employees)
}

const getEmployee = (req, res) => {

    const employee = data.employees.find(x => x.id === parseInt(req.body.id))
    if (!employee) {
        return res.status(400).json({ 'message': `Employee ID ${req.body.id} not found` })
    }

    res.json(employee)
}

module.exports = {
    getAllEmployees,
    createNewEmployee,
    updateEmployee,
    deleteEmployee,
    getEmployee
}