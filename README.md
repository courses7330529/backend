RESTful API

Tech Stack:
- Node/Express.js
- MongoDB + Mongoose
  

- Simple RESTful API designed with MVC pattern.
- Event and error Logger.  
- Authentication and Authorization implemented using JWT.
- Role verification