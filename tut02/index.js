// const fs = require('fs')
const fsPromises = require('fs').promises
const path = require('path')



const fileOps = async () => {
    try {
        const data = await fsPromises.readFile(path.join(__dirname, 'starter2.txt'), 'utf8')
        console.log(data);
    } catch (error) {
        console.error(err);
    }
}

fileOps()

//'./tut02/starter.txt'
// fs.readFile(path.join(__dirname, 'starter.txt'), 'utf8', (err, data) => {
//     if (err) throw err
//     // console.log(data.toString());
//     console.log(data)
//     console.log('read complete')
// })

console.log('ASYNC OPERACII!');

// // CALLBACK HELL
// fs.writeFile(path.join(__dirname, 'starter.txt'), 'az sum sakata ot writeFile', (err) => {
//     if (err) throw err
//     // console.log(data.toString());
//     console.log('write complete')

//     fs.appendFile(path.join(__dirname, 'starter.txt'), 'az sum sakata ot appendFile callback hell', (err) => {
//         if (err) throw err
//         // console.log(data.toString());
//         console.log('append complete')
//     })

//     fs.rename(path.join(__dirname, 'starter.txt'), path.join(__dirname, 'starter2.txt'), (err) => {
//         if (err) throw err
//         // console.log(data.toString());
//         console.log('rename complete')
//     })
// })

// fs.appendFile(path.join(__dirname, 'starter.txt'), 'az sum sakata ot appendFile', (err) => {
//     if (err) throw err
//     // console.log(data.toString());
//     console.log('append complete')
// })

process.on('uncaughtException', err => {
    console.error(`There was an uncaught error: ${err}`);
    process.exit(1)
})