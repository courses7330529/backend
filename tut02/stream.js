const fs = require('fs')

const rs = fs.createReadStream('./tut02/lorem.txt', { encoding: 'utf8' })

const ws = fs.createWriteStream('./tut02/new-lorem.txt')
rs.on('data', dataChunk => {
    ws.write(dataChunk)
})

// rs.pipe(ws)